<?php
// Register Custom Taxonomy
function ldm_mark_portfolio_for_vault() {

    $labels = array(
        'name'                       => _x( 'Vault Status', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Vault Status', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Vault Status', 'text_domain' ),
        'all_items'                  => __( 'All', 'text_domain' ),
        'parent_item'                => __( 'Parent Type', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Type:', 'text_domain' ),
        'new_item_name'              => __( 'New Vault Status', 'text_domain' ),
        'add_new_item'               => __( 'Add Vault Status', 'text_domain' ),
        'edit_item'                  => __( 'Edit Vault Status', 'text_domain' ),
        'update_item'                => __( 'Update Vault Status', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
        'search_items'               => __( 'Search Vault Status', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove Vault Statuses', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used Vault Statuses', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'portfolio_in_vault', array( 'portfolio' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'ldm_mark_portfolio_for_vault', 0 );