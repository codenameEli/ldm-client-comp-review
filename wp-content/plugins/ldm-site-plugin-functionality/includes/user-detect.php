<?php

add_action( 'init', 'ldm_detect_user' );

function ldm_detect_user() {
	global $oUserAccessManager;
	global $post;

	if (isset($oUserAccessManager)) {
		$iPostId = $post->ID;
		$oUamAccessHandler = $oUserAccessManager->getAccessHandler();
		$aGroups = $oUamAccessHandler->getUserGroupsForObject('post', $iPostId);
        if (count($aGroups) > 0) {
            $sLink = TXT_UAM_ASSIGNED_GROUPS.': ';
            foreach ($aGroups as $oGroup) {
                $sLink .= $oGroup->getGroupName().', ';
            }
            $sLink = rtrim($sLink, ', ');
        }
        // error_log( print_r( $oUamAccessHandler, true ) );
		return $sLink;
	}
}