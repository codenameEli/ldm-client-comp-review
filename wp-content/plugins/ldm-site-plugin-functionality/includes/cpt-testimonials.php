<?php
// Register Custom Post Type
function cpt_testimonials() {

    $labels = array(
        'name'                => _x( 'Testimonials', 'Post Type General Name', 'text_domain' ),
        'singular_name'       => _x( 'Testimonial', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'           => __( 'Testimonials', 'text_domain' ),
        'parent_item_colon'   => __( 'Parent Testimonial:', 'text_domain' ),
        'all_items'           => __( 'All Testimonials', 'text_domain' ),
        'view_item'           => __( 'View Testimonial', 'text_domain' ),
        'add_new_item'        => __( 'Add New Testimonial', 'text_domain' ),
        'add_new'             => __( 'Add New', 'text_domain' ),
        'edit_item'           => __( 'Edit Testimonial', 'text_domain' ),
        'update_item'         => __( 'Update Testimonial', 'text_domain' ),
        'search_items'        => __( 'Search Testimonial', 'text_domain' ),
        'not_found'           => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
    );
    $args = array(
        'label'               => __( 'testimonial', 'text_domain' ),
        'description'         => __( 'Testimonials that show up on the homepage', 'text_domain' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
        'taxonomies'          => array( ),
        'hierarchical'        => false,
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => false,
        'show_in_admin_bar'   => true,
        'menu_position'       => 34,
        'menu_icon'           => get_stylesheet_directory_uri() . '/launchdm/images/logo-color.png',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => false,
        'capability_type'     => 'page',
    );
    register_post_type( 'testimonial', $args );

}

// Hook into the 'init' action
add_action( 'init', 'cpt_testimonials', 0 );