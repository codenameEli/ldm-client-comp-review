<?php
/**
 * Product CPT Metaboxes
 * using https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'display_team_member_information' );

/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function display_team_member_information( array $meta_boxes ) {
    $meta_boxes['team_member_information'] = array(
        'id'         => 'team-member-information',
        'title'      => __( 'Team Member Information', 'launchdm' ),
        'pages'      => array( 'team-member',),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'fields'     => array(
            array(
            	'name' => __( '', 'cmb' ),
            	'desc' => __( 'LaunchDM Extra Team Photo', 'cmb' ),
            	'id'   => 'team_member_extra_photo',
            	'type' => 'text_url',
            ),
            array(
            	'name' => __( '', 'cmb' ),
            	'desc' => __( 'Upload an image or enter a URL.', 'cmb' ),
            	'id'   => 'time_machine_photo',
            	'type' => 'file',
            ),
        )
    );

    return $meta_boxes;
}