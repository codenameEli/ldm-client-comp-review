<?php
/*
 * Template Name: Client Comp
 */

// Make sure they are logged in, if not redirect to login page
// auth_redirect();

//* Remove the entry title (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

add_action( 'genesis_after_header', 'ldm_display_single_client_comp' );

function ldm_display_single_client_comp() {
	global $post;

	$attr = array(
		'class'	=> "featured-image client-comp",
	);

	$comp = get_the_post_thumbnail( $post->ID, null, $attr );

	echo $comp;
}

genesis();