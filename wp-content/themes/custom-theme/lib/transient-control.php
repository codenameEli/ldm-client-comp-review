<?php

function ldm_transient($transient_name='', $query_args='') {
	global $post;
	$the_transient = get_transient( $transient_name );

	if ( false === $the_transient )  {
		$custom_query = new WP_Query( $query_args );
		set_transient( $transient_name, $custom_query, 12 * HOUR_IN_SECONDS );
		$the_transient = get_transient( $transient_name );
	}

	return $the_transient;
}

// TRANSIENT DELETE
// Slideshow
add_action( 'edit_post', 'ldm_hero_slideshow_clear_transient' );
function ldm_hero_slideshow_clear_transient( $post_id ) {
	if ( 'hero_slideshow' !== get_post_type( $post_id ) ) {return;}
	delete_transient( 'hero-slideshow' );
}