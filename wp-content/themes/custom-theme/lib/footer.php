<?php

add_action( 'genesis_footer', 'ldm_display_footer' );

add_filter('genesis_footer_creds_text', 'ldm_footer_creds_text');

function ldm_display_footer() {

	// $output = '<div class="footer-menu-container row">';
	// 	$output .= ldm_get_footer_menu( '1' );
	// 	$output .= ldm_get_footer_menu( '2' );
	// 	$output .= ldm_get_footer_menu( '3' );
	// 	$output .= ldm_get_footer_menu( '4' );
	// $output .= '</div>';

	// $output .= '<div class="social-media-icons-container row">';
	// 	$output .= ldm_get_social_media_menu();
	// $output .= '</div>';

	// echo $output;
}

function ldm_get_footer_menu( $number ) {

	// $args = array(
	// 	'theme_location' => '',
	// 	'menu' => "footer-menu-$number",
	// 	'container' => 'div',
	// 	'container_class' => "menu-container-$number span3",
	// 	'container_id' => '',
	// 	'menu_class' => 'menu',
	// 	'menu_id' => '',
	// 	'echo' => false,
	// 	'fallback_cb' => 'wp_page_menu',
	// 	'before' => '',
	// 	'after' => '',
	// 	'link_before' => '',
	// 	'link_after' => '',
	// 	'items_wrap' => '<ul id = "%1$s" class = "%2$s clearfix">%3$s</ul>',
	// 	'depth' => 0,
	// 	'walker' => ''
	// );

	// return wp_nav_menu( $args );
}

function ldm_get_social_media_menu() {

	// $args = array(
	// 	'theme_location' => '',
	// 	'menu' => "social-media",
	// 	'container' => 'div',
	// 	'container_class' => "menu-container span12",
	// 	'container_id' => '',
	// 	'menu_class' => 'menu',
	// 	'menu_id' => '',
	// 	'echo' => false,
	// 	'fallback_cb' => 'wp_page_menu',
	// 	'before' => '<i class="ldmfa-icon">',
	// 	'after' => '</i>',
	// 	'link_before' => '',
	// 	'link_after' => '',
	// 	'items_wrap' => '<ul id = "%1$s" class = "%2$s fa-ul clearfix">%3$s</ul>',
	// 	'depth' => 0,
	// 	'walker' => ''
	// );

	// return wp_nav_menu($args);
}

function ldm_footer_creds_text($creds) {
	// $creds = '<span class="footer-creds">[footer_copyright] ' . get_bloginfo('name') . ' &verbar; Website Design by: <a href="//launchdm.com/web-design-philadelphia.html" target="_blank">LaunchDM</a></span>';
	// return $creds;
}