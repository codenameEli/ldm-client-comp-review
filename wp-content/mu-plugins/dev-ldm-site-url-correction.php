<?php
/*
Plugin Name: Dev LDM Site URL Correction
Plugin URI: http://launchdm.com
Description: Correct Site URL on the fly for development purposes only
Author: LaunchDM
Version: 1.1
Author URI: http://launchdm.com

1.1 
in the output included the current site_url() value as a link
added wp_redirect after a new site_url is set

*/

function ldm_site_url_correction() {
    // build URL used in request
    $requested_url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

    // ONLY take action if the site_url does not appear in the requested_url
    if ( stripos($requested_url, site_url()) === false ) { 

        // truncate the string at the first ? (ignore url parameters)
        $requested_url = current(explode('?', $requested_url)); 

        // remove trailing slash (/) if there is one
        $requested_url = ( substr($requested_url, -1)==='/' ?  substr($requested_url, 0, -1) : $requested_url );

        // URL paramter indicates we are to update based on the requested url
        if ( isset($_GET['set-site-url']) &&  $_GET['set-site-url'] === 'true') {
            update_option('siteurl', $requested_url); 
            update_option('home', $requested_url);
            wp_redirect( home_url() ); exit;

            // if set-site-url true (via URL parameter)
        } else {
            // the site-site-url was not set to true
            // there is no indication we should update the site url
            // list possible options
            ?>
                <br />
                <h1>The site_url() does NOT match the requested url</h1>
                <h2>Current site_url(), <a href="<?php echo site_url(); ?>"><?php echo site_url(); ?></a></h2>
                <h2>You can update the site url by choosing the correct url below:</h2>
                <?php
                echo "<ul>\n";
                $exploded = explode('/', $requested_url); // break up the requested_url by the slashses
                for ($i=2; $i<sizeof($exploded); $i++) { // note: start at second slash (/) due to http:// (it will never be that or shorter)
                    $possible_url = '';
                    for ($j=0; $j<=$i; $j++) {
                        // build possible url by combining exploded parts with slashes inbetween
                        $possible_url .= $exploded[$j].'/';
                    } // for j
                    echo "<li>";
                    echo '<a href="'.$possible_url.'?set-site-url=true">'; // create url with url parameter to approve change
                    echo $possible_url;
                    echo "</a>";
                    echo "</li>";
                } // for i
                echo "</ul>\n";
                exit();
        }
    } // if ( site_url is NOT within $requested_url)
}

add_action( 'plugins_loaded', 'ldm_site_url_correction' );

