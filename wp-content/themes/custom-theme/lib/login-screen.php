<?php
//BEGIN Change Login Logo 300x125
function ldm_login_logo() {
    $logo = get_stylesheet_directory_uri() . '/images/launch-logo.png';

    echo '<style type="text/css">
        html {
            background-color: #fff;
        }
    	body {
    		background-color: #fff;
            font: 14px Georgia,Times,"Times New Roman",serif;
            line-height: 19px;
            color: #564E31;
    	}

        a, .login #backtoblog a, .login #nav a {
            color: #B2BB1C!important;
            font: 13px Georgia,Times,"Times New Roman",serif;
            line-height: 22px;
            text-decoration: none;
        }

        a:hover {
            text-decoration: underline;
        }

        .launchbar {
            height: 50px;
            width: 100%;
            background-color: #222222;
        }
        .launch-logo {
            width: 132px;
            height: 40px;
            margin-left: 20px;
            background: url("'. $logo . '");
        }
    	.login label {
    		color: #827f75!important;
    	}
    	.login #backtoblog a, .login #nav a {
    		color: #564E31;
    	}
    	.login #backtoblog a:hover, .login #nav a:hover, .login h1 a:hover {
    		color: #fff;
    		text-decoration: underline;
    	}
    	.wp-core-ui .button-group.button-large .button, .wp-core-ui .button.button-large {
    		background: #554e31;
            padding: 2px 35px;
    		border: 0;
            border-radius: 0;
            -webkit-border-radius: 0;
            text-transform: uppercase;
    		border-color: rgba(0, 0, 0, 0);
    		-webkit-box-shadow: inset 0 1px 0 rgba(43, 75, 19, 0.65);
    		box-shadow: inset 0 1px 0 rgba(43, 75, 19, 0.65);
    	}
    	input[type=checkbox]:checked:before {
    		color: #B2BB1C;
    	}
    	input[type=checkbox]:focus, input[type=email]:focus, input[type=number]:focus, input[type=password]:focus, input[type=radio]:focus, input[type=search]:focus, input[type=tel]:focus, input[type=text]:focus, input[type=url]:focus, select:focus, textarea:focus {
    		border-color: #B2BB1C;
    		-webkit-box-shadow:0 0 2px rgba(43, 75, 19, 0.65);
    		box-shadow:0 0 2px rgba(43, 75, 19, 0.65);
    	}
    	.wp-core-ui .button-primary.focus, .wp-core-ui .button-primary:focus {
    		border-color: #B2BB1C;
    		-webkit-box-shadow:0 0 2px rgba(43, 75, 19, 0.65);
    		box-shadow:0 0 2px rgba(43, 75, 19, 0.65);
    	}
    	#login {
    		z-index: 9;
    		position: relative;
            padding: 15px 0;
    	}
        .login .message {
            border-left: 4px solid #B2BB1C;
        }

    	#loginform {
    		background: #f3f1ec;
            box-shadow: none;
            -webkit-box-shadow: none;
    	}
        .login h1 {
    		display: none;
        }

        .login-message {
            text-align: center;
            display: block;
            margin-top: 40px;
        }

        .login form .input, .login form input[type=checkbox], .login input[type=text] {
            color: #564E31;
        }

        .footer-container {
            text-align: center;
            position: relative;
            margin-top: 15px;
        }

        .footer-container:before {
            content: "";
            display: block;
            top: -15px;
            height: 1px;
            width: 80%;
            background-color: #dedede;
            margin: 0 auto;
            position: absolute;
            left: 50%;
            margin-left: -40%;
        }

        .footer-container span {
            display: block;
            color: #564E31;
            font: 13px Georgia,Times,"Times New Roman",serif;
            line-height: 22px;
        }
    </style>';

    echo '<div class="navbar launchbar"><div class="launch-logo"></div></div>';
    echo '<span class="login-message">Welcome to your design portal. To view your designs please login below.</span>';

    echo $footer;
}
add_action('login_head', 'ldm_login_logo');
add_action('login_footer', 'ldm_login_footer');

function ldm_login_footer() {
    $footer = '<div class="footer-container">';
        $footer .= '<span class="est-97">EST. 97 | CREATE ART</span>';
        $footer .= '<span class="address">© 2014 LaunchDM | 828 Penn Ave., Wyomissing, PA 19610 | 610-898-1330 | For more info please <a href="http://launchdm.com/contact-us.html">contact us.</a></span>';
    $footer .= '</div>';

    echo $footer;
}
add_filter( 'login_headerurl', 'ldm_login_url' );
function ldm_login_url($url) {
	return get_site_url();
}
//END Change Login Logo
