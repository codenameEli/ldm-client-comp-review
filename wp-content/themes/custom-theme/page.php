<?php
// This is the default page
// Because we cannot detect user groups programmatically
// We just read the visible nav based on the user

// Make sure they are logged in, if not redirect to login page
// auth_redirect();

// remove default primary widget area
remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );

add_action( 'genesis_before_content', 'ldm_display_client_comps_links' );

function ldm_display_client_comps_links() {
	global $post;
	$walker = new LDM_Menu_With_Description;

	echo '<p class="description">Click on the darker boxes at the top of each column to view the most recent designs. Click on the lighter boxes to view previous designs.';

	$defaults = array(
		'theme_location'  => '',
		'menu'            => 'client list menu',
		'container'       => 'div',
		'container_class' => '',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'depth'           => 0,
		'walker'          => $walker
	);

	$menu = wp_nav_menu( $defaults );
}

genesis();
